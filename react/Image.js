import React, { Component } from 'react'

class Image extends Component {
    render() {
        const {
            url,
            title,
        } = this.props

        return (
            <img src={url} alt={title} className="av-image-component" />
        )
    }
}