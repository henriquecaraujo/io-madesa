import React, { Component } from 'react'

class TextSeo extends Component {
    render() {
        return (
            <div className="av-seo-text pa3">
            <div className="tc">
                <h1 className="fw4 dib tl">Ambientes <span className="db fw6 lh-title">completos</span></h1>
            </div>
                <p className="tracked ph3 f2 lh-copy">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel justo vitae nisi tempor cursus eget at ex.
                    Donec euismod elit turpis, nec pharetra quam feugiat nec
                    Nam at justo in ligula interdum hendrerit. Fusce scelerisque, turpis sed tempus pretium, neque magna dapibus nisl.
                </p>
            </div>
        )
    }
}

export default TextSeo